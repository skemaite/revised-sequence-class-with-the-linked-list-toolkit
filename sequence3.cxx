// FILE: sequence3.cxx
// CLASS IMPLEMENTED: sequence (see sequence3.h for documentation)
//1.The items in the sequence are stored in a linked list.
//2.The header pointer of the list is stored in the member
//variable head_ptr.
//3.The total number of items in the list is stored in the
//member variable many_nodes.

#include <iostream>
#include <cassert> // Provides assert function
#include "node1.h"
#include "sequence3.h"

using namespace std;
namespace main_savitch_5
{
	sequence::sequence() //linked list: O(1) dynamic array: O(n)
    {
    	many_nodes = 0;
        head_ptr = NULL;
        tail_ptr = NULL;
        cursor = NULL;
        precursor = NULL;
    } 

	sequence::sequence(const sequence& source) //linked list: O(n) dynamic array: O(n)
	{
		list_copy(source.head_ptr, head_ptr, tail_ptr);
		many_nodes = source.many_nodes;
        
        if(source.cursor==NULL)
        {
            cursor=NULL;
            precursor=NULL;
        }
        else if (source.precursor==NULL)
        {
            cursor= head_ptr;
            precursor=NULL;
        }
        else
        {
            node* temporary_ptr=source.head_ptr;
            cursor=head_ptr;
            precursor=NULL;
            while(temporary_ptr!=source.cursor){
                temporary_ptr=temporary_ptr->link();
                precursor=cursor;
                cursor=cursor->link();
        }
	}
	}

//sequence::~sequence linked list: O(n) dynamic array: O(n)
	sequence::~sequence() 
	{
		many_nodes = 0;
        list_clear(head_ptr);
        tail_ptr = NULL;
        cursor = NULL;
        precursor = NULL;
	}


   //sequence::start linked list: O(1) dynamic array: O(n)
	void sequence::start ( ) 
	{
        
        cursor = head_ptr;
        precursor = NULL;
	}


//sequence::size linked list: O(1) dynamic array: O(n)
        sequence::size_type sequence::size( ) const
        {
        		return many_nodes;
        }


//sequence::is_item linked list: O(1) dynamic array: O(n)
        bool sequence::is_item( ) const 
        {
            bool item = true;
            if(cursor == NULL)
            {
                item = false;
            }
            return item; 
        }


//sequence::current linked list: O(1) dynamic array: O(n)
        sequence::value_type sequence::current( ) const
        {
        	assert(is_item());
            return cursor->data();
        }

//sequence::advance linked list: O(1) dynamic array: O(n)
    void sequence::advance( )
    {
        assert(is_item());
        precursor = cursor;
        cursor = cursor->link();
    }


//sequence::insert linked list: O(1) dynamic array: O(n)
    void sequence::insert(const value_type& entry)
    {
    	if(cursor == head_ptr || !is_item())
    	{
    		list_head_insert(head_ptr, entry);
            cursor=head_ptr;
            precursor=NULL;
            if(many_nodes == 0)
                tail_ptr=head_ptr;
    	}
    	else
    	{
    		list_insert(precursor,entry);
            cursor=precursor->link();
    	}
        ++many_nodes;
    	
    }

//sequence::attach linked list: O(1) dynamic array: O(n)
    void sequence::attach(const value_type& entry)
    {
        	
    if(!is_item())
    	{
           if(many_nodes == 0)
           {
                list_head_insert(head_ptr, entry);
                cursor=head_ptr;
                tail_ptr=head_ptr;
           }
    	   else
            {
                precursor=tail_ptr;
                list_insert (precursor, entry);          
                tail_ptr = tail_ptr->link();
                cursor=tail_ptr;
            }
        }
    else
    {
        precursor=cursor;
        list_insert(cursor, entry);
        cursor=cursor->link();
        if(precursor==tail_ptr)
            tail_ptr=tail_ptr->link();
     }

     ++many_nodes;
    	
    	
    }

//sequence::operator= linked list: O(n) dynamic array: O(n)
    void sequence::operator=(const sequence& source)
    {
    	if (this == &source)
    	   return;
        else
        {
            list_clear(head_ptr);
            list_copy(source.head_ptr, head_ptr, tail_ptr);
            many_nodes = source.many_nodes;
        }
    	   	
        
        if(source.cursor==NULL)
        {
            cursor=NULL;
            precursor=NULL;
        }
        else if (source.precursor==NULL)
        {
            cursor= head_ptr;
            precursor=NULL;
        }
        else
        {
            node* temporary_ptr=source.head_ptr;
            cursor=head_ptr;
            precursor=NULL;
            while(temporary_ptr!=source.cursor){
                temporary_ptr=temporary_ptr->link();
                precursor=cursor;
                cursor=cursor->link();
		}
        }
    }

//sequence::remove_current linked list: O(1) dynamic array: O(n)
    void sequence::remove_current( )
    {
        assert(is_item());
        if(cursor == head_ptr)
        {
            cursor=cursor->link();
            list_head_remove(head_ptr);
            precursor=NULL;
        }
        else
        {
            cursor=cursor->link();
            list_remove(precursor);
        }	
        --many_nodes;        

    }
}